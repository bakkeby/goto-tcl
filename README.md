# goto Tcl #

This is a Tcl library that implements the [goto](https://en.wikipedia.org/wiki/Goto) statement allowing transfer of execution flow to arbitrary points in the code denoted by labels.

Needless to say `goto` statements is a feature that is *sorely* missed by the Tcl community.

It is worth noting that one limitation with this implementation is that labels and goto statements need to be placed inside Tcl procedures (`proc`s).

### Syntax ###

**Labels:**

```
# label: <label name>
```

The label name cannot contain spaces and as such consists of the first word following the fixed comment string "label:" (case sensitive).

Indentation is allowed.

**goto statement:**
```
goto <label name>
```

Due to the simplicity of the implementation of the goto logic one basic limitation is that labels and goto calls must be placed within Tcl procedures.

### How do I get set up? ###

* download goto Tcl, unpack, and ensure that the path to the folder is included in the ::auto_path
* include `package require goto 1.0` at the start of your elegant Tcl script
* add `# label: X` within a procedure
* add `goto X` statement(s) within a procedure
* try to predict how the code will behave
* run the script and see if you were right
* also have a look at [goto.test](https://bitbucket.org/bakkeby/goto-tcl/src/master/goto.test?at=master&fileviewer=file-view-default) for other examples

### Examples ###

Consider the complicated syntax of the following for loop:

```tcl
for {set i 0} {$i < 10} {incr i} {
	puts "i = $i"
}
```

this you can now write as:

```tcl
set i 0
# label: A
puts "i = $i"
incr i
if {$i < 10} {
	goto A
}
```
so much easier right?

### What are the benefits of using goto statements in Tcl? ###

* code obfuscation for your script is built-in
* reduced performance (allowing you to actually read and follow the logs as they are printed)
* put spokes in the wheels of structured programming
* allows you to be even more retro than Tcl, in Tcl!
* break the taboo and improve your geek status by meddling with the occult
* make your code unmaintainable by others (and most likely yourself)
* become the only one in the company that has even the slightest idea of what is going on

### License ###

* Simplified BSD License/FreeBSD License (GPL-compatible free software license)

### Requirements ###

* Tcl8.6