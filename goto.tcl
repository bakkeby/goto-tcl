package provide goto 1.0

namespace eval goto {
	variable GOTO
}

rename proc _proc

_proc proc { name params body } {
	set body_lines [split $body \n]
	
	set l [llength $body_lines]
	for {set i 0} {$i < $l} {incr i} {
		if {[regexp {# *label: *([^ ]+)} [lindex $body_lines $i] --> label]} {
			set ::goto::GOTO($label) [join [lrange $body_lines $i+1 end] \n]
		}
	}
	_proc ${name} $params $body
}
	
proc goto { label } {
	
	if {![info exists ::goto::GOTO($label)]} {
		error "Goto label $label does not exist"
	}
	
	tailcall try $::goto::GOTO($label)
}